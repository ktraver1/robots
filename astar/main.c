/* Dijkstra's Algorithm in C */
#include<stdio.h>
#include<conio.h>
#include<process.h>
#include<string.h>
#include<math.h>
#include <stdlib.h>


typedef int bool;
#define true 1
#define false 0
#define X_MAX 10
#define Y_MAX 10
#define START_X 0
#define START_Y 0
#define GOAL_X 0
#define GOAL_Y 0
#define MAX 101
#define TOTAL_NOT_FREE 1

typedef struct
{
    int xPos;
    int yPos;
    //eaxct cost to reach node from starting node
    int g;
    //cost to reach end(goal)
    int h;
    //check if obstlce or end of course
    bool isFree;
    struct Node* parent;
}Node;

Node grid[X_MAX][Y_MAX];


int calculate_heuristic(int x,int y)
{
    int result = sqrt(pow((x - GOAL_X), 2) + pow((y - GOAL_Y), 2));

    return result;
}
//calcultate the totalt cost to reach the end
int calculate_f_cost(Node node)
{
    int result = node.g+node.h;
    return result;
}
//see if 2 nodes are the same
bool compare(Node node1, Node node2)
{
    if((node1.xPos == node2.xPos)&&(node1.yPos == node2.yPos))
       {
           return true;
       }
       else
       {
            return false;
       }
}
Node free_neighbor(Node node,int direction)
{
    int x,y;
    Node result;
    //top
    if(direction ==0)
    {
        x = node.xPos;
        y = node.yPos;
        x++;
        if(x <= X_MAX)
        {
             result = grid[x][y];
        }

    }
    //right
    else if(direction ==1)
    {
        x = node.xPos;
        y = node.yPos;
        y++;
        if(y <= Y_MAX)
        {
             result = grid[x][y];
        }
    }
    //bottom
    else if(direction ==2)
    {
        x = node.xPos;
        y = node.yPos;
        x--;
        if(x >= 0)
        {
             result = grid[x][y];
        }
    }
    //left
    else if(direction ==2)
    {
        x = node.xPos;
        y = node.yPos;
        y--;
        if(y >= 0)
        {
             result = grid[x][y];
        }
    }
    return result;
}
void A_star(Node Start)
{
    //add starting square to openlist
    Node open_list[100];
    Node closed_list[100];
    Node current;

    Node successor;

    int open_size = 0;
    int closed_size = 0;
    open_list[0] = Start;
    open_size++;
    //loop as long as somting is in the open lsit
    int i, temp_index,f,old_f;
    bool goal_reached = false;
    bool inClosed = false;
    bool inOpen = false;
    bool deadEnd = true;
    //bool obstcle = true;
    while(!goal_reached)
    {
        temp_index = 0;
        f = 0;
        //cost can never be more than 101
        old_f = MAX;
        //while(obstcle)
        //{

            for(i = 0; i < open_size; i++)
            {
                f = calculate_f_cost(open_list[i]);
                if(f < old_f)
                {
                    temp_index = i;
                }
            }
            //move to space if obscle than go back remove from open_list and use diffrent node
            //if new obstcle or cross out of bounds than go back to paretn make that one no free and loop through again
            //else obstlce=false
        //}
        //have index of current node, the one with best path
        current = open_list[temp_index];
        //check if current is the goal

        if(current.h == 0)
        {
            goal_reached = true;
        }
        else
        {
            //add current to closed list
            closed_list[closed_size] = current;
            closed_size++;
            //look at its neighbors
            //has 4 neighbors up down left right
            //check if viable location
            deadEnd = true;
            for(i = 0; i < 4; i++)
            {
                successor = free_neighbor(current,i);
                if(successor.isFree)
                {
                    inClosed = false;
                    inOpen = false;
                    //the neighbor is valid and can be reached
                    //no known obstcle and in bounds
                    for(i = 0; i < closed_size; i++)
                    {
                        //check if neigbor is in closed list
                        if(compare(closed_list[i],successor))
                        {
                            if(closed_list[i].g > successor.g)
                            {
                               inClosed = true;

                               successor.parent = current;
                               closed_list[i].g = successor.g;
                            }

                        }
                    }
                    //check if in open if not in closed
                    if(!inClosed)
                    {


                        for(i = 0; i < open_size; i++)
                        {
                            //check if neigbor is in closed list
                            if(compare(open_list[i],successor))
                            {

                                if(open_list[i].g > successor.g)
                                {
                                   inOpen = true;
                                   successor.parent = current;
                                   open_list[i].g =successor.g;
                                }
                            }
                        }
                    }
                    //not in open or closed
                    if(!inClosed && !inOpen)
                    {
                        successor.parent = current;
                        open_list[open_size] = successor;
                        open_size++;
                    }
                }
            }
            if(deadEnd)
            {
                open_list[open_size] = current.parent;
                open_size++;
                current.isFree = false;
            }
        }
    }



}
int main()
{
    //create graph
    //call a_star
    int x,y,i;
    //set num of know obstcles and create array of them
    int num_known_obstcles = TOTAL_NOT_FREE;
    Node obstcleArray[num_known_obstcles];
    //save them inton array

    obstcleArray[0].xPos = 3;
    obstcleArray[0].yPos = 3;

    for(x = 0; x < X_MAX; x++)
    {
        for(y = 0; y <Y_MAX; y++)
        {
            grid[x][y].xPos = x;
            grid[x][y].yPos = y;
             grid[x][y].isFree = true;
            for(i = 0; i<num_known_obstcles;i++)
            {
                if(compare(grid[x][y],obstcleArray[i]))
                {
                   grid[x][y].isFree = false;
                }

            }
            grid[x][y].h = calculate_heuristic(x,y);
            grid[x][y].g = 1;
        }
    }

    A_star(grid[START_X][START_Y]);

    return 0;
}

